from gettext import find
from tp2 import *

# on veut pouvoir creer des boites
def test_box_create ():
    b = Box()

# on veut pouvoir mettre des trucs dedans
def test_box_add ():
    b = Box()
    b.add("truc1")
    b.add("truc2")

def test_contains():
    b = Box()
    assert not "truc1" in b
    b.add("truc1")
    assert "truc1" in b 
    b.retirer_box("truc1")
    assert not "truc1" in b 

def test_retirer_box():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.retirer_box("truc1")
    assert "truc1" not in b
    try:
        b.retirer_box("machin")
    except:
        assert "Une box innexistante a essayer d'etre retirer"

def test_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.retirer_box("truc1")
    assert "truc1" not in b

def test_remove_inexistant():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.retirer_box("truc1")
    assert "truc1" not in b
    try:
        b.retirer_box("machin")
        assert False # On n'atteint pas cette ligne car
                     # une exception est levée
    except BoxContentError:
        pass


def test_open():
    b = Box()
    assert b.is_open()
    b.close()
    assert not b.is_open() 
    b.open()
    assert b.is_open()

def test_look():
    b = Box()
    b.open()
    assert b.action_look() == "il n'y a rien dans la boite"
    b.add(Chose("truc", 1))
    b.add(Chose("truc", 2))
    b.add(Chose("truc", 3))
    b.add(Chose("truc", 4))
    b.add(Chose("truc", 5))
    assert b.action_look() == "la boite contient: truc volume : 1, truc volume : 2, truc volume : 3, truc volume : 4, truc volume : 5"
    b.close()
    assert b.action_look() == "la boite est fermée"

def test_thing():
    b = Box()
    b.open()
    for i in range(5):
        b.add(Chose("truc", i))
    assert not b.thing(5)
    assert b.thing(3) == "truc volume : 3"

def test_capacité():
    b = Box()
    assert b.get_capacity() == None
    b.set_capacity(5)
    assert b.get_capacity() == 5

def test_has_room_for():
    b = Box()
    b.set_capacity(5)
    b.add(Chose('truc', 3))
    b.add(Chose('truc', 1))
    assert not b.has_room_for(Chose('truc', 2))
    assert b.has_room_for(Chose('truc', 1))

def test_action_add():
    b = Box()
    b.set_capacity(5)
    b.add(Chose('truc', 3))
    b.add(Chose('truc', 1))
    b.action_add(Chose("truc", 2))

def test_has_name():
    c = Chose('bidule', 3)
    assert c.has_name('bidule')
    c.set_name('truc')
    assert not c.has_name('bidule')
    assert c.has_name('truc')
    
def testfind():
    b = Box()
    b.open()
    b.add(Chose("truc", 1))
    b.add(Chose("truc", 2))
    b.add(Chose("truc", 3))
    b.add(Chose("bidule", 4))
    b.add(Chose("truc", 5))
    c = b.find('bidule')
    assert c.getNom() == 'bidule'

def testUtil():
    u = Utilisateur(1, "Prades", "Matteo")
    assert u.getID() == 1
    assert u.getNom() == "Prades"
    assert u.getPrenom() == "Matteo"
    testPrint = u.__repr__()
    assert testPrint == "1 Prades Matteo"
    u.creerBox("boxMatteo")
    u.creerBox("boxMatteo1")
    assert u.getboxs() == ["boxMatteo", "boxMatteo1"]

def testSalle():
    sTests = dict()
    u = Utilisateur(1, "Prades", "Matteo")
    u.creerSalle(sTests, 1)
    u.creerSalle(sTests, 2)
    u.creerSalle(sTests, 3)
    u.creerSalle(sTests, "salleVide")
    sTestsKeys = []
    for salle in sTests.keys():
        sTestsKeys.append(salle)
    assert sTestsKeys == [1, 2, 3, "salleVide"]
    u.creerBox(1)
    u.creerBox(2)
    u.creerBox(22)
    u.creerBox(3)
    u.creerBox(32)
    u.creerBox(33)
    u.creerBox('test')

    u.placerBox(1, u.getbox(1), sTests)
    u.placerBox(2, u.getbox(2), sTests)
    u.placerBox(2, u.getbox(22), sTests)
    u.placerBox(3, u.getbox(3), sTests)
    u.placerBox(3, u.getbox(32), sTests)
    u.placerBox(3, u.getbox(33), sTests)
    salle1 = sTests[1]
    salleTest = Salle('test')
    u.placerBox(1, u.getbox('test'), None, salleTest)
    salle2 = sTests[2]
    salle3 = sTests[3]
    salleVide = sTests["salleVide"]
    print(salle1.getBoxs())
    print(salle2.getBoxs())
    print(salle3.getBoxs())
    print(salleTest.getBoxs())
    print(salle1)
    print(salle2)
    print(salle3)
    print(salleVide)
    assert salle1.getBoxs() == ["1"]
    assert salle2.getBoxs() == ["2", "22"]
    assert salle3.getBoxs() == ["3", "32", "33"]
    u.retirerBox(sTests, 3, "32")
    assert salle3.getBoxs() == ["3", "33"]
    print(salle3)

testSalle()