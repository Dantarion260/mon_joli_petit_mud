from hashlib import new
from operator import truediv
import yaml

class Chose:
    def __init__(self, nom = 'name', volume = 0):
        self._volume = volume
        self._nom = nom
    
    def getVolume(self):
        return self._volume

    def getNom(self):
        return self._nom

    def getchose(self):
        return str(self._nom) + " " + "volume : " + str(self._volume)

    def __repr__(self):
        return self.getchose()

    def set_name(self, nom):
        self._nom = nom
    
    def has_name(self, nom):
        return self.getNom() == nom

class BoxContentError(Exception):
    pass

class BoxTooFullError(Exception):
    pass


class Box:
    def __init__(self, nomBox = "nom", ouverte = True, capacity = None):
        self._contents = []
        self._ouverte = ouverte
        self._capacity = capacity
        self._nom = str(nomBox)
    
    def __repr__(self):
        if not self._ouverte:
            return "la boite est fermée"

        elif len(self._content) == 0:
            return "la boite est vide."

        else:
            res = "la boite contient: "
            for ind in range(len(self._contents) - 1):
                res += self._content[ind].getNom() + "(" + self._content[ind].getVolume() + ") / "
        res += self._content[-1].getNom() + "(" + self._content[-1].getVolume() + "). "
        return res

    def add(self , truc ):
        self._contents .append(truc)

    def __contains__(self, machin):
        for truc in self._contents:
            if truc == machin:
                return True
        return False
    
    def retirer_box(self, a_retirer):
        if a_retirer not in self:
            raise BoxContentError()
        self._contents.remove(a_retirer)
        
    def open(self):
        self._ouverte = True
        
    def close(self):
        self._ouverte = False

    def is_open(self):
        return self._ouverte

    def action_look(self):
        res = "la boite contient: "
        if self._ouverte and len(self._contents) != 0:
            res += str(self._contents[0].getchose())
            for objet in self._contents:
                if objet != self._contents[0]:
                    res += ", " + str(objet)
            return res

        elif self._ouverte and len(self._contents) == 0:
            return "il n'y a rien dans la boite"

        return "la boite est fermée"

    def thing(self, volume):
        if self.is_open():
            for truc in self._contents:
                if truc.getVolume() == volume:
                    return truc.getchose()

    def set_capacity(self, capacité):
        self._capacity = capacité

    def get_capacity(self):
        return self._capacity

    def getNom(self):
        return self._nom

    def has_room_for(self, chose):
        if self.get_capacity() != None:
            restant = self.get_capacity()
            for truc in self._contents:
                restant -= truc.getVolume()
            
            restant -= chose.getVolume()
            if restant < 0:
                return False
        return True

    def action_add(self, chose):
        if self.is_open():
            if self.has_room_for(chose):
                if self.get_capacity() != None:
                    self.set_capacity(self.get_capacity() - chose.getVolume())
                self.add(chose)

    def find(self, nom):
        if self.is_open():
            for chose in self._contents:
                if chose.has_name(nom):
                    return chose
        return None

    @staticmethod
    def  from_yaml(namefile):
        """permet de lire un fichier yml

        Args:
            namefile (str): un .yml
        """
        with open(namefile) as file:
            data = yaml.load(file, Loader=yaml.FullLoader)
            

class Utilisateur:

    def __init__(self, ID, Nom, Prenom):
        self._ID = ID
        self._nom = Nom
        self._prenom = Prenom
        self._boxUtil = dict()

    def getID(self):
        return self._ID

    def getNom(self):
        return self._nom

    def getPrenom(self):
        return self._prenom

    def getboxs(self):
        """Récupere la liste des boites crée par l'utilisateur

        Returns:
            _type_: list
        """
        res = []
        for box in self._boxUtil.keys():
            res.append(box)
        return res

    def getbox(self, nom):
        """Récupere une box dans les boxs crée par l'utilisateur

        Args:
            nom (str): le nom de la boite

        Returns:
            _type_: Box
        """
        return self._boxUtil[nom]

    def __repr__(self):
        return str(self.getID()) + " " + self.getNom() + " " + self.getPrenom() 

    def creerBox(self, nomBox, ouverte = True, capacité = None):
        """crée une box et la met dans la liste des boxs crée par l'utilisateur

        Args:
            nomBox (str): le nom de la boite crée
            ouverte (bool, optional): si elle est ouverte ou non. Defaults to True.
        """
        self._boxUtil[nomBox] = Box(nomBox, ouverte, capacité)

    def creerSalle(self, baseDonneesSalles, IDsalle):
        """crée une salle

        Args:
            baseDonneesSalles (Dict): la base de donnée des salles
            IDsalle (int): l'id de la salle en question 
        """
        baseDonneesSalles[IDsalle] = Salle(IDsalle)

    def placerBox(self, IDsalle, box, baseDonneesSalles = None, salle = None):
        """place une box dans le dictionnaire des boxs de la salle placer en parametre

        Args:
            IDsalle (int): l'id de la salle qui prendra la box
            box (Box): La box
            baseDonneesSalles (dict, optional): la base de donnée ou est stocké la salle qui prendra la box pour si on utilise pas une salle directement. Defaults to None.
            salle (Salle, optional): La salle qui prendra la box si on a pas de base de donnée (pour les tests). Defaults to None.
        """
        nomBox = box.getNom()
        if baseDonneesSalles != None:
            baseDonneesSalles[IDsalle].ajouterBox(nomBox, box)
        else:
            salle.ajouterBox(nomBox, box)

    def retirerBox(self, baseDonneesSalles, IDSalle, nomBox):
        """  retire une box de la salle donnée

        Args:
            baseDonneesSalles (dict): le dictionnaire ou sont stockés les salles
            IDSalle (int): l'id de la salle
            nomBox (str): le nom de la box
        """
        baseDonneesSalles[IDSalle].retirerBox(nomBox)
        


class Salle:

    def __init__(self, ID, listeBox = None):
        self._ID = ID
        if listeBox == None:
            self._listeBox = dict()
        else:
            self._listeBox = listeBox

    def __repr__(self):
        if len(self._listeBox) == 0:
            return "la salle " + str(self._ID) + " est vide."

        else:
            res = "la salle " + str(self._ID) + " contient " + str(len(self._listeBox)) +" boxs:"
            for box in self._listeBox.keys():
                res += box + " / "
        return res

    def ajouterBox(self, nomBox, box):
        """ajoute un box a la salle

        Args:
            nomBox (str): le nom de la box
            box (Box): la Box
        """
        self._listeBox[nomBox] = box

    def retirerBox(self, nomBox):
        """retire une box de la salle

        Args:
            nomBox (str): le nom de la box
        """
        self._listeBox.pop(nomBox)

    def getID(self):
        return self._ID

    def getBoxs(self):
        """récupere la liste des boxs présentes dans la salle

        Returns:
            list: la liste des boxs présentes dans la salle 
        """
        res = []
        for box in self._listeBox.keys():
            res.append(box)
        return res


print(Box.from_yaml("boiteyml.yml"))